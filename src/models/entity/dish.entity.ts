import {
  Table,
  Column,
  Model,
  HasOne,
  PrimaryKey,
  ForeignKey
} from "sequelize-typescript";
import CustomModel from "../../common/custom.model";
import { ApiModelProperty } from "@nestjs/swagger";

@Table({
  tableName: "dishes"
})
export class Dish extends CustomModel<Dish> {
  @ApiModelProperty()
  @Column
  type: string;

  @ApiModelProperty()
  @Column
  name: string;

  @ApiModelProperty()
  @Column
  cost: number;

  @ApiModelProperty()
  @Column
  description: string;
}
