import {Component} from "@nestjs/common";
import {Dish} from "../models/entity/dish.entity";

@Component()
export class DishService {
  constructor() {}

  async createDish(dish: Dish): Promise<Dish> {
    const newDish = Dish.create<Dish>(dish);
    return newDish;
  }
  async updateDish(dish: Dish): Promise<Dish> {
    if (!dish.id) {
      throw new Error("Dish id is null");
    }
    const editedDish: Dish = await Dish.findById<Dish>(dish.id);
    return editedDish.update(dish);
  }
  async saveDish(dish: Dish): Promise<Dish> {
    return dish.id ? this.updateDish(dish) : this.createDish(dish);
  }
}
