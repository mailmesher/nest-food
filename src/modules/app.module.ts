import {
  Module,
  MiddlewaresConsumer,
  RequestMethod
} from "@nestjs/common";
import { UsersController } from "../controllers/users.controller";
import { DatabaseModule } from "../models/database.module";
import { AuthController } from "../controllers/auth.controller";
import { SessionMiddleware } from "../common/session.middleware";
import { DishesController } from "../controllers/dishes.controller";
import { AnyExceptionFilter } from "../common/filters/exception.filter";
import { LoggerMiddleware } from "../common/logger.middleware";
import { loggerProviders } from "../common/providers/logger.provider";
import { UserService } from "../services/user.service";
import { DishService } from "../services/dish.service";
import { userProviders } from "../models/entity/user.entity";
import { profilesProviders } from "../models/entity/profile.entity";

@Module({
  modules: [DatabaseModule],
  controllers: [UsersController, AuthController, DishesController],
  components: [
    UserService,
    DishService,
    AnyExceptionFilter,
    ...loggerProviders,
    ...userProviders,
    ...profilesProviders
  ]
})
export class ApplicationModule {
  configure(consumer: MiddlewaresConsumer): void {
    const allRouts = {
      path: "/**",
      method: RequestMethod.ALL
    };
    consumer.apply(SessionMiddleware).forRoutes(allRouts);
    consumer.apply(LoggerMiddleware).forRoutes(allRouts);
  }
}
