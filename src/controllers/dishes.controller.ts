import { Post, Body, Req, Controller, Get } from "@nestjs/common";
import { DishService } from "../services/dish.service";
import { Dish } from "../models/entity/dish.entity";
import { ApiConsumes, ApiUseTags } from "@nestjs/swagger";

@ApiUseTags("dishes")
@Controller("dishes")
export class DishesController {
  constructor(private readonly dishService: DishService) {}

  @Post("save")
  async login(@Body() dish: Dish, @Req() req) {
    const savedDish = await this.dishService.saveDish(dish);
    return savedDish;
  }

  @Get("list")
  async list() {
    return await Dish.findAll<Dish>();
  }
}
