import { Controller, Get } from "@nestjs/common";
import { UserService } from "../services/user.service";
import { ApiUseTags } from "@nestjs/swagger";

@ApiUseTags("users")
@Controller("users")
export class UsersController {
  constructor(private readonly usersService: UserService) {}

  @Get()
  async findAll() {}
}
