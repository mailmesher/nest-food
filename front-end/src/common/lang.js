// collect keys in single object
// translate be use this object for return value ru: Второе;
import * as commonLang from "./common.lang.json";

export default class Lang {
  static lang = "ru";

  dictionary;

  constructor(localDictionary) {
    this.dictionary = Object.assign(commonLang, localDictionary);
  }

  get(key) {
    return {
      value: key,
      text: this.dictionary[key]
        ? this.dictionary[key][Lang.lang]
        : key
    };
  }
  trans(key) {
    return this.dictionary[key]
      ? this.dictionary[key][Lang.lang]
      : key;
  }
}
