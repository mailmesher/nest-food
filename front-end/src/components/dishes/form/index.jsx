import React from "react";
import {
  Loader,
  Form,
  Button,
  Message,
  Dropdown,
  Divider,
  TextArea
} from "semantic-ui-react";
import "./styles.less";
import * as localLang from "../list/lang.json";
import Lang from "../../../common/lang";

export default class DishForm extends React.Component {
  lang = new Lang(localLang);

  types = [
    this.lang.get("FIRST_DISH"),
    this.lang.get("SECOND_DISH"),
    this.lang.get("GARNISH")
  ];

  render() {
    return (
      <Form loading={this.props.loading} onSubmit={this.handleSubmit}>
        <Form.Field>
          <label>Название</label>
          <input
            required
            ref={input => (this.nameInput = input)}
            placeholder="Название"
          />
        </Form.Field>
        <Form.Field>
          <label>Описание</label>
          <TextArea
            required
            ref={input => (this.descriprionTextArea = input)}
            placeholder="Описание"
            autoHeight
          />
        </Form.Field>
        <Form.Group>
          <Form.Field>
            <label>Цена</label>
            <input
              required
              type="number"
              ref={input => (this.costInput = input)}
              placeholder="Цена"
            />
          </Form.Field>
          <Form.Field>
            <label>Тип</label>
            <Dropdown
              required
              ref={input => (this.selectType = input)}
              placeholder={"Тип блюда"}
              selection
              options={this.types}
            />
          </Form.Field>
        </Form.Group>
        <Divider />
        <Button type="submit">Submit</Button>
      </Form>
    );
  }

  handleSubmit = event => {
    this.props.onSubmit({
      type: this.selectType.state.value,
      name: this.nameInput.value,
      cost: this.costInput.value,
      description: this.descriprionTextArea.ref.value
    });
  };
}
