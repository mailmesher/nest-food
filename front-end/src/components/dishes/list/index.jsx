import React from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
// import "./styles.less";
import {
  Divider,
  Button,
  Modal,
  Header,
  Icon,
  Segment,
  Dimmer,
  Loader
} from "semantic-ui-react";
import { connect } from "react-redux";
import { compose } from "redux";
import reducer from "./reducer";
import saga from "./saga";
import {
  addModalOpen,
  saveDishRequest,
  listDishesRequest
} from "./actions";
import injectReducer from "../../../../utils/injectReducer";
import injectSaga from "../../../../utils/injectSaga";
import DishForm from "../form";
import ReactTable from "react-table";
import Lang from "../../../common/lang.js";
import * as localLang from "./lang.json";
import "./styles.less";

class DishesList extends React.Component {
  lang = new Lang(localLang);

  componentDidMount() {
    this.props.dispatch(listDishesRequest());
  }
  render() {
    // this.props.loaderDishesList
    const columns = [
      {
        Header: this.lang.trans("NAME"),
        accessor: "name"
      },
      {
        Header: this.lang.trans("DISH_TYPE"),
        accessor: "type",
        Cell: props => <span>{this.lang.get(props.value).text}</span>
      },
      {
        Header: this.lang.trans("DESCRIPTION"),
        accessor: "description"
      }
    ];

    return (
      <div className="dishes-list">
        <ReactTable
          data={this.props.dishes}
          columns={columns}
          defaultPageSize={10}
          getTdProps={(state, rowInfo, column, instance) => {
            return {
              onClick: (e, handleOriginal) => {
                this.onEditItem(rowInfo.original);
              }
            };
          }}
          className="entityManager -highlight"
        />
        {this.renderAdd()}
      </div>
    );
  }
  onRemoveItem = removedItem => {
    console.log(removedItem);
  };
  onEditItem = editedItem => {
    console.log(editedItem);
  };

  renderAdd() {
    return (
      <div>
        <Modal
          size={"mini"}
          open={this.props.addModalOpen}
          onClose={() => this.props.dispatch(addModalOpen(false))}
        >
          <Modal.Header>Создание блюда</Modal.Header>
          <Modal.Content>
            <DishForm
              loading={this.props.loaderAddDish}
              onSubmit={this.onDishSaveSubmit}
            />
          </Modal.Content>
        </Modal>
        <Button
          icon
          primary
          size="small"
          onClick={() => this.props.dispatch(addModalOpen(true))}
        >
          Add Dish <Icon name="plus" />
        </Button>
      </div>
    );
  }
  onDishSaveSubmit = dish => {
    this.props.dispatch(saveDishRequest(dish));
  };
}
const withConnect = connect(state => {
  return state.get("dishes").toJS();
});
const withSaga = injectSaga({ key: "dishes", saga });
const withReducer = injectReducer({
  key: "dishes",
  reducer
});
export default compose(withReducer, withSaga, withConnect)(
  DishesList
);
