import {
  ADD_MODAL_SWITCH,
  SAVE_DISH_REQUEST,
  SAVE_DISH_RESPONSE,
  LIST_DISHES_REQUEST,
  LIST_DISHES_RESPONSE
} from "./actions";
import { fromJS, List } from "immutable";
import { combineReducers } from "redux-immutable";
import { Response } from "../../../common/constants";

const initialState = fromJS({
  addModalOpen: false,
  loaderAddDish: false,
  loaderDishesList: false,
  dishes: []
});

export default function dishes(state = initialState, action) {
  switch (action.type) {
    case ADD_MODAL_SWITCH:
      return state.set("addModalOpen", action.isOpen);

    case SAVE_DISH_REQUEST:
      return state.set("loaderAddDish", true);
    case SAVE_DISH_RESPONSE:
      return state
        .set("loaderAddDish", false)
        .set("addModalOpen", false);
    case LIST_DISHES_REQUEST:
      return state.set("loaderDishesList", true);
    case LIST_DISHES_RESPONSE:
      if (action.status == Response.SUCCESS) {
        return state
          .set("loaderDishesList", false)
          .update("dishes", dishes => List().concat(action.data));
      }
    default:
      return state;
  }
}
