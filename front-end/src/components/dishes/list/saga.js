import {
  call,
  put,
  select,
  takeLatest
} from "redux-saga/effects";
import {
  SAVE_DISH_REQUEST,
  SAVE_DISH_RESPONSE,
  LIST_DISHES_REQUEST,
  LIST_DISHES_RESPONSE
} from "./actions";
import request from "../../../../utils/request";
import axios from "axios";
import { push } from "react-router-redux";
import { saveDishSuccess } from "./actions";
import { Response } from "../../../common/constants";

export function* saveDish(action) {
  try {
    const response = yield call(
      axios.post,
      "/api/dishes/save",
      action.dish
    );
    yield put({
      type: SAVE_DISH_RESPONSE,
      status: Response.SUCCESS,
      response
    });
  } catch (e) {
    // yield put(
    //   loginFailure(e.response.data.message || e.message)
    // );
  }
}
export function* listDishes(action) {
  try {
    const response = yield call(axios.get, "/api/dishes/list");
    yield put({
      type: LIST_DISHES_RESPONSE,
      status: Response.SUCCESS,
      data: response.data
    });
  } catch (e) {
    // yield put(
    //   loginFailure(e.response.data.message || e.message)
    // );
  }
}
/**
 * Root saga manages watcher lifecycle
 */
export default function* saveDishAttempt() {
  yield takeLatest(SAVE_DISH_REQUEST, saveDish);
  yield takeLatest(LIST_DISHES_REQUEST, listDishes);
}
