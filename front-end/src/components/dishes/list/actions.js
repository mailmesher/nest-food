import { Response } from "../../../common/constants";
export const ADD_MODAL_SWITCH = "pages/dishes/ADD_MODAL_SWITCH",
  addModalOpen = isOpen => {
    return {
      type: ADD_MODAL_SWITCH,
      isOpen
    };
  },
  /* save dishes*/

  SAVE_DISH_REQUEST = "pages/dishes/SAVE_DISH_REQUEST",
  SAVE_DISH_RESPONSE = "pages/dishes/SAVE_DISH_RESPONSE",
  saveDishRequest = dish => {
    return {
      type: SAVE_DISH_REQUEST,
      dish
    };
  },
  /* fetch dishes list*/

  LIST_DISHES_REQUEST = "pages/dishes/LIST_DISHES_REQUEST",
  LIST_DISHES_RESPONSE = "pages/dishes/LIST_DISHES_RESPONSE",
  listDishesRequest = () => {
    return {
      type: LIST_DISHES_REQUEST
    };
  };
