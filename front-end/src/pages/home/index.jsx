import React from "react";
import axios from "axios";
import {
  Button,
  Message,
  Segment,
  Menu,
  Container,
  Image,
  Dropdown,
  Header,
  Icon,
  Grid,
  List,
  Divider
} from "semantic-ui-react";
import WeekPage from "./week";
import OrdersPage from "./orders";
import DishesPage from "./dishes";

import {
  Redirect,
  BrowserRouter,
  Route,
  Link,
  Router,
  Switch
} from "react-router-dom";
import "./styles.less";
import { logoutRequest } from "../../common/global/global.actions";
import { connect } from "react-redux";
import { compose } from "redux";

class HomePage extends React.Component {
  basePath = "/home";

  componentDidMount() {}

  logout = () => {
    this.props.dispatch(logoutRequest());
  };
  render() {
    const activeRoute = this.props.activeRoute,
      weekLink = this.basePath + "/week",
      ordersLink = this.basePath + "/orders",
      dishesLink = this.basePath + "/dishes";
    return (
      <div className="home-layout">
        <div>
          <Menu pointing secondary>
            <Link to={weekLink}>
              <Menu.Item
                as="div"
                name="week"
                active={activeRoute === weekLink}
                onClick={this.handleItemClick}
              />
            </Link>
            <Link to={ordersLink}>
              <Menu.Item
                as="div"
                name="orders"
                active={activeRoute === ordersLink}
                onClick={this.handleItemClick}
              />
            </Link>
            <Link to={dishesLink}>
              <Menu.Item
                as="div"
                name="dishes"
                active={activeRoute === dishesLink}
                onClick={this.handleItemClick}
              />
            </Link>
            <Menu.Menu position="right">
              <Menu.Item name="logout" onClick={this.logout} />
            </Menu.Menu>
          </Menu>
        </div>
        <div className="home-body">
          <Switch>
            <Route path={"*/orders"} component={OrdersPage} />
            <Route path={"*/week"} component={WeekPage} />
            <Route path={"*/dishes"} component={DishesPage} />
            <Redirect to={dishesLink} />
          </Switch>
        </div>
      </div>
    );
  }

  getLayoutTemplate() {}

  componentDidMount() {}

  handleItemClick = (e, { name }) => {
    this.setState({ activeRoute: name });
  };
}
const withConnect = connect(state => {
  return {
    activeRoute: state.getIn(["route", "location", "pathname"])
  };
});
export default compose(withConnect)(HomePage);
