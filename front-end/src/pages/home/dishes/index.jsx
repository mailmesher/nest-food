import "./styles.less";
import React from "react";
import DishesList from "../../../components/dishes/list";

export default class DishesPage extends React.Component {
  render() {
    return (
      <div>
        <DishesList />
      </div>
    );
  }
}
