import React from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
import "./styles.less";
import OrdersList from "../../../components/ordersList";

export default class OrdersPage extends React.Component {
  render() {
    return (
      <div className="orders-layout">
        <OrdersList />
      </div>
    );
  }
  componentDidMount() {}
}
